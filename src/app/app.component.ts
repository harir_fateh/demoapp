import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DemoApp';
  mysql = "No data loaded, yet";
  constructor(private http: HttpClient) 
  { 
    this.http.get('/demo', {responseType: 'text'}).subscribe((mysql: any) => {
      console.log(mysql);
	  this.mysql = mysql;		
	});
}
}
