FROM node:16-alpine3.12 as builder

WORKDIR /app

COPY package*.json ./

RUN npm install --force

COPY . .

RUN npm run build -- --outputPath=./dist/out --configuration=production

FROM nginx:alpine

# Copier les fichiers de construction de l'étape précédente
COPY --from=builder /app/dist/out /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
